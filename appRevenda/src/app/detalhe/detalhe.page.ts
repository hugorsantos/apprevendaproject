import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides, NavController, NavParams } from '@ionic/angular';
import { ICarro } from '../interfaces/icarro';

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.page.html',
  styleUrls: ['./detalhe.page.scss'],
})
export class DetalhePage implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;
  public carro: ICarro;
  public imagemPadrao:string = 'https://maxcdn.icons8.com/Share/icon/Transport//car1600.png';

  constructor(public route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.carro = this.router.getCurrentNavigation().extras.state.carro;
      }
    });
  }

  ngOnInit() {
    
  }

  anteriorSlide(){
    this.slides.slidePrev();
  }
  proximoSlide(){
    this.slides.slideNext();
  }

}

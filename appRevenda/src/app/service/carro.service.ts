import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { ICarro } from '../interfaces/icarro';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarroService {

  public url = 'http://127.0.0.1:8000/api/carros';
  private carrosSubjects: BehaviorSubject<ICarro[]> = new BehaviorSubject<ICarro[]>(null);

  constructor(public http: HttpClient) {
  }

  listaCarros(): Observable<ICarro[]>{
    this.http.get<ICarro[]>(this.url).subscribe(this.carrosSubjects);
    return this.carrosSubjects.asObservable();
  }


}

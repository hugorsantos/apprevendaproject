import { Component } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ICarro } from '../interfaces/icarro';
import { CarroService } from '../service/carro.service';
import { DetalhePage } from '../detalhe/detalhe.page';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public listaCarros: ICarro[];
  public imagemPadrao: string = 'https://maxcdn.icons8.com/Share/icon/Transport//car1600.png';


  constructor(
    public router: Router,
    public servico: CarroService) {
    this.servico.listaCarros().subscribe(
      data => {
        this.listaCarros = data;
      }
    );
  }

  abreDetalhe(carro: ICarro){
    const navigationExtras: NavigationExtras = {
      state: {
        carro: carro
      }
    };
    this.router.navigate(['detalhe'], navigationExtras);
  }

}
